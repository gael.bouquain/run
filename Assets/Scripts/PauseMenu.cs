﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool gameIsPaused = false;

    public GameObject pauseMenuUI;
    public GameObject settingsWindow;
    public GameObject[] elementsToHide;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (gameIsPaused)
            {
                Resume();
                CloseSettingsWindow();
            }
            else if (!gameIsPaused)
            {
                Paused();
            }
        }
    }
    void Paused()
    {
        Cursor.visible = true;

        PlayerMovement.instance.enabled = false;
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0;
        gameIsPaused = true;
    }

    public void Resume()
    {
        Cursor.visible = false;

        PlayerMovement.instance.enabled = true;
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1;
        gameIsPaused = false;
    }

    public void LoadMainMenu()
    {
        DontDestroyOnLoadScene.instance.RemoveFromDontDestroyOnLoad();
        Resume();
        Cursor.visible = true;
        SceneManager.LoadScene("Main Menu");
    }
    public void SettingsButton()
    {
        settingsWindow.SetActive(true);
        for (int i = 0; i < elementsToHide.Length; i++)
        {
            elementsToHide[i].SetActive(false);
        }
    }
    public void CloseSettingsWindow()
    {
        settingsWindow.SetActive(false);
        for (int i = 0; i < elementsToHide.Length; i++)
        {
            elementsToHide[i].SetActive(true);
        }
    }
}
