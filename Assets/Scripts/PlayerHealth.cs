﻿using System.Collections;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    public int maxPlayerHealth;
    public int currentHealth;

    public float invinsibleFlashDelay;
    public bool isInvinsible = false;
    public float InvincibilityDelay;

    public HealthBar healthBar;
    public SpriteRenderer graphics;

    public AudioClip hitSound;

    public static PlayerHealth instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instace de PlayerHealth dans la scène");
        }
        instance = this;
    }

    private void Start()
    {
        currentHealth = maxPlayerHealth;
        healthBar.SetMaxHealth(maxPlayerHealth);
        healthBar.SetHealth(maxPlayerHealth);
    }

    public void ResetPV()
    {
        currentHealth = maxPlayerHealth;
        healthBar.SetMaxHealth(maxPlayerHealth);
        healthBar.SetHealth(maxPlayerHealth);
    }

    public void SetMaxHealth(int _maxHealth)
    {
        maxPlayerHealth = _maxHealth;
        currentHealth = _maxHealth;
        healthBar.SetMaxHealth(maxPlayerHealth);
    }

    public void TakeDamage(int _health)
    {
        if (!isInvinsible)
        {
            if (currentHealth - _health <= 0)
            {
                Die();
                healthBar.SetHealth(0);
                return;
            }

            AudioManager.instance.PlayClipAt(hitSound, transform.position);
            currentHealth -= _health;
            isInvinsible = true;
            StartCoroutine(InvincibilityFlash());
            StartCoroutine(HandleInvincibilityDelay());

            healthBar.SetHealth(currentHealth);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            TakeDamage(60);
        }
    }

    public void Die()
    {
        //PlayerSpawn.instance.RespawnPlayerWithAnimte();
        //currentHealth = 100;
        PlayerMovement.instance.enabled = false;
        PlayerMovement.instance.animator.SetTrigger("Die");
        PlayerMovement.instance.rb.bodyType = RigidbodyType2D.Kinematic;
        PlayerMovement.instance.rb.velocity = Vector3.zero;
        PlayerMovement.instance.playerCollider.enabled = false;
        GameOverManager.instance.OnPlayerDeath();
    }

    public void Respawn()
    {
        //PlayerSpawn.instance.RespawnPlayerWithAnimte();
        //currentHealth = 100;
        PlayerMovement.instance.enabled = true;
        PlayerMovement.instance.animator.SetTrigger("Respawn");
        PlayerMovement.instance.rb.bodyType = RigidbodyType2D.Dynamic;
        PlayerMovement.instance.playerCollider.enabled = true;
        currentHealth = maxPlayerHealth;
        healthBar.SetHealth(maxPlayerHealth);
    }

    public void HealPlayer(int _amount)
    {
        if (currentHealth + _amount > maxPlayerHealth)
        {
            currentHealth = maxPlayerHealth;
        }
        else
        {
            currentHealth = maxPlayerHealth;
        }

        healthBar.SetHealth(currentHealth);
    }

    public IEnumerator InvincibilityFlash()
    {
        while (isInvinsible)
        {
            graphics.color = new Color(1f, 1f, 1f, 0f);
            yield return new WaitForSeconds(invinsibleFlashDelay);
            graphics.color = new Color(1f, 1f, 1f, 1f);
            yield return new WaitForSeconds(invinsibleFlashDelay);
        }
    }

    public IEnumerator HandleInvincibilityDelay()
    {
        yield return new WaitForSeconds(InvincibilityDelay);
        isInvinsible = false;
    }
}
