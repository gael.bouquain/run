﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioClip[] playlist;
    public AudioSource audioSource;
    public int musicIndex = 0;

    public AudioMixerGroup soundEffectMixer;

    public static AudioManager instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instace de AudioManager dans la scène");
        }
        instance = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        audioSource.clip = playlist[0];
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!audioSource.isPlaying)
        {
            PlayNextSong();
        }
    }

    private void PlayNextSong()
    {
        musicIndex = (musicIndex + 1) % playlist.Length;
        audioSource.clip = playlist[musicIndex];
        audioSource.Play();
    }

    public AudioSource PlayClipAt(AudioClip _clip, Vector3 _pos)
    {
        // on créer GameObject vide
        GameObject tempGO = new GameObject("TempAudio");
        
        // on lui applique la position
        tempGO.transform.position = _pos;

        // on ajoute au GameObject vide un component (AudioSource) et on le stock dans une variable
        AudioSource audioSource = tempGO.AddComponent<AudioSource>();

        // on charge le son/clip
        audioSource.clip = _clip;

        // on change la sortie du son
        audioSource.outputAudioMixerGroup = soundEffectMixer;

        //on joue le son
        audioSource.Play();

        // et on détruit le GameObject quand le son est terminé
        Destroy(tempGO, _clip.length);

        // et on retourne l'object
        return audioSource;
    }
}
