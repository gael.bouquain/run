﻿using UnityEngine;

public class WaterSlowDown : MonoBehaviour
{
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            collision.attachedRigidbody.AddForce(new Vector2(0f, 20));
        }
    }
}
