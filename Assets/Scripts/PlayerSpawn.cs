﻿using System.Collections;
using UnityEngine;

public class PlayerSpawn : MonoBehaviour
{
    private Animator fadeSystem;

    public HealthBar HealthBar;

    public static PlayerSpawn instance;

    private void Awake()
    {
        if (instance != null)
        {
            Debug.LogWarning("Il y a plus d'une instace de PlayerSpawn dans la scène");
        }

        instance = this;

        fadeSystem = GameObject.FindGameObjectWithTag("FadeSystem").GetComponent<Animator>();


        GameObject.FindGameObjectWithTag("Player").transform.position = transform.position;
    }

    public void RespawnPlayer()
    {
        GameObject.FindGameObjectWithTag("Player").transform.position = transform.position;
        HealthBar.SetHealth(100);
    }

    public void RespawnPlayerWithAnimte()
    {
        StartCoroutine(RespawnAnimate());
        HealthBar.SetHealth(100);
    }

    private IEnumerator RespawnAnimate()
    {
        fadeSystem.SetTrigger("FadeIn");
        yield return new WaitForSeconds(1f);
        GameObject.FindGameObjectWithTag("Player").transform.position = transform.position;
    }
}
